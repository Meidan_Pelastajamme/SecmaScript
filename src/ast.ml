type expr =
  | Int of int
  | Str of string
  | Ident of string

type statement = 
  | Func of string

type prog = statement list

(*
  | Ident of ident
  | Bool of bool
  | Prefix of 
    { op: token
    ; right: expression 
    }
  | Infix of 
    { left: expression
    ; op: token
    ; right: expression 
    }
  | FnCall of 
    { name: identifier
    ; args: expression list 
    }
  | List of expression list

type ident = string

type statement =
  | FnDecl of 
    { name: ident
    ; params: ident list
    ; body: expr 
    }
  | VarDecl of 
    { name: ident
    ; datatype: datatype
    ; value: expr 
    }
  | VarMut of 
    { name: ident
    ; value: expr 
    }
  | Block of statement list
*)
