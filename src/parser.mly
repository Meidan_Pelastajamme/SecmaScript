%{
open Ast
%}

%token <int> INT
%token <string> STR
%token <string> IDENT
%token <string> FUNC
%token EOF

%type <Ast.expr> expr
%type <Ast.prog> prog
%type <Ast.statement> statement
%start prog

%%

prog:
  | statement EOF { [$1] }
  | statement prog { [$1] @ $2 }
  | EOF { failwith "empty" }
  ;

statement: 
  | FUNC STR { Func $2 }
  ;

expr: 
  | INT { Int $1 } 
  | STR { Str $1 }
  | IDENT { Ident $1 }
  ;

