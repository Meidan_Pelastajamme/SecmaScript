open Ast
open Helpers

let parse (s: string): prog =
  let lexbuf = Lexing.from_string s in
  let ast = Parser.prog Lexer.read lexbuf in
  ast

let _print_expr expr =
  match expr with
  | Int x -> print_endline ("INT: " ^ (string_of_int x))
  | Str s -> print_endline ("STR: " ^ s)
  | Ident i -> print_endline ("IDENT: " ^ i)

let rec print_prog (prog: prog) =
  match prog with
  | Func name::rest -> print_endline ("FUNC NAME: " ^ name); print_prog rest
  | [] -> ()
;;
let () =
  let input = read_file_to_string "input.ss" in
  print_prog (parse input);
