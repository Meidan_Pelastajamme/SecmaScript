{
open Parser
}

let digit = ['0'-'9']
let int = '-'? digit+

let alpha = ['a'-'z' 'A'-'Z']

let ident_start = alpha | '_'
let ident_body = alpha | digit | alpha | '_'

let skipped = ' '+ | '\n'+ | "\r\n"+

rule read = parse 
  | skipped { read lexbuf }
  | int { INT (int_of_string (Lexing.lexeme lexbuf)) }
  | '"' { let buffer = Buffer.create 256 in 
    STR (read_str buffer lexbuf)
  }
  | "func" { FUNC (Lexing.lexeme lexbuf) }
  | ident_start ident_body+ { IDENT (Lexing.lexeme lexbuf) }
  | eof { EOF }

and read_str buffer = parse
  | '"' { Buffer.contents buffer }
  | "\\t" { Buffer.add_char buffer '\t'; read_str buffer lexbuf }
  | "\\n" { Buffer.add_char buffer '\t'; read_str buffer lexbuf }
  | '\\' '"' { Buffer.add_char buffer '"'; read_str buffer lexbuf }
  | '\\' '\\' { Buffer.add_char buffer '\\'; read_str buffer lexbuf }
  | eof { Buffer.contents buffer }
  | _ as char { Buffer.add_char buffer char; read_str buffer lexbuf }
