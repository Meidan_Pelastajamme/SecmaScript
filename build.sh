#!/bin/sh

mkdir -p _build
cp -r src/* _build/

cd _build/
ocamllex lexer.mll
ocamlyacc parser.mly

# TODO: Factor out in a for loop all the files to compile AND link
ocamlc -c parser.mli
ocamlc -c lexer.ml
ocamlc -c parser.ml
ocamlc -c ast.ml
ocamlc -c helpers.ml 
ocamlc -c main.ml

ocamlc -o main ast.cmo helpers.cmo lexer.cmo parser.cmo main.cmo 

cd ..

